var http = require('http');

var port = process.env.PORT || 1337;
http.createServer(function(req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end('<h2>Hello World</h2>');
}).listen(port, function() {
    console.log('server is running on http://localhost:' + port);
});
