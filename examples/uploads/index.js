var express = require('express');
var multer = require('multer');
var move = require('mv');
var upload = multer({
  dest: '/tmp'
})


var app = express();

app.get('/', function(req, res) {
  res.send('hello world');
});

app.post('/', upload.array('images'), function(req, res) {
  var files = req.files;
  files.forEach(function(file) {
    var source = file.path;
    var dest = './uploads';
    var fileName = file.originalname;
    move(source, dest + '/' + fileName, function(err) {
      if (err) {
        throw err;
      }
      console.log(fileName + ' is uploaded successfully');
    });
  });
  res.status(204).end();
});

app.listen(8080, function cb() {
  console.log('upload app is runing now on 8080');
});
