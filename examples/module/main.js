//main.js

var square = require('./square');
var rect = require('./rectangle');
var circle = require('./circle');


console.log('Area of square is : ',square.area(5));
console.log('Area of rectangle : ', rect.area(5,10));
console.log('Area of circle : ', circle.area(10));
